-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--     year                  -- year of partition to compute statistics for.
--     month                 -- month of partition to compute statistics for.
--
-- Usage:
--     spark3-sql --master yarn -f publish_geoeditors_weekly.hql          \
--         -d destination_directory=/wmf/tmp/differential_privacy/example \
--         -d month='2023-05'                                             \
--         -d coalesce_partitions=1

SET spark.hadoop.hive.exec.compress.output=false;

WITH weeks AS (
    SELECT DISTINCT
        DATE_SUB(date, EXTRACT(dayofweek FROM date) - 1) as week_start
    FROM
        wmf.editors_daily
    WHERE
        month = '${month}'
)

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    -- Set 0 as volume column since we don't use it.
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' '\t')

    SELECT /*+ COALESCE(${coalesce_partitions}) */
        gw.wiki_db,
        CONCAT(CONCAT(w.language_code, '.'), w.database_group) as project,
        c.name as country_name,
        gw.country_code,
        gw.edit_range,
        gw.count_eps,
        gw.sum_eps,
        gw.count_release_thresh,
        gw.private_count,
        gw.private_sum,
        gw.week_start
    FROM differential_privacy.geoeditors_weekly gw
    JOIN weeks
    ON weeks.week_start = gw.week_start
    JOIN canonical_data.countries c 
    ON c.iso_code = gw.country_code
    JOIN canonical_data.wikis w
    ON w.database_code = gw.wiki_db
;

