import argparse

from differential_privacy.utils.editor_metrics import calc_error_geoeditors

from pyspark.sql import functions as sf
from pyspark.ml.feature import Bucketizer
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, FloatType

from tmlt.analytics.privacy_budget import PureDPBudget
from tmlt.analytics.query_builder import QueryBuilder
from tmlt.analytics.keyset import KeySet
from tmlt.analytics.session import Session
from tmlt.analytics.binning_spec import BinningSpec
from tmlt.analytics.protected_change import AddOneRow

#------------ HYPERPARAMETERS AND CONSTANTS ------------#
# epsilons and release thresh
param_dict = {
    'Lower risk': {
        'count_eps': 1.1,
        'sum_eps': 0.9,
        'release_thresh': 8
    },
    'Medium risk': {
        'count_eps': 0.2,
        'sum_eps': None,
        'release_thresh': 45
    },
    'Higher risk': {
        'count_eps': 0.1,
        'sum_eps': None,
        'release_thresh': 95
    },
}

# binning-related things
bin_col = 'edit_range' # name of column with ranges
splits = [0., 4., 99., float('inf')]
edit_binspec = BinningSpec(splits)
labels = edit_binspec.bins()

# short-hand lists to save space referring to further down
input_cols = ['sum_edits']
private_output_cols = ['private_count', 'private_sum']
nonprivate_output_cols = [ 'nonprivate_count', 'nonprivate_sum']

# keys to join private/nonprivate dfs on for error calculation
join_keys = ['country_code', 'wiki_db', bin_col]

#------------ SCHEMAS ------------#
error_schema = StructType([ 
    StructField('data_risk_classification', StringType(), False),
    StructField('name', StringType(), False),
    StructField('released', IntegerType(), True),
    StructField('med_rel_err', FloatType(), True),
    StructField('dropped', FloatType(), True),
    StructField('spurious', FloatType(), True),
    StructField('rel_err_fraction_0_5', FloatType(), True),
    StructField('rel_err_fraction_0_9', FloatType(), True),
    StructField('total_inconsistent', IntegerType(), True),
    StructField('pct_inconsistent', FloatType(), True),
    StructField('time_frame', StringType(), False)
])

#------------ SQL QUERIES ------------#
# all country-wikidb combos we are calculating noise for
keyset_query = """
SELECT
  c.iso_code as country_code,
  w.database_code as wiki_db
FROM canonical_data.countries c
CROSS JOIN canonical_data.wikis w
WHERE
  c.data_risk_classification = '{data_risk_classification}'
  AND w.status = 'open'
  AND w.visibility = 'public'
  AND w.editability = 'public'
  AND w.database_group IN (
    'wikipedia',   -- 320 instances
    'wiktionary',  -- 166 instances
    'wikibooks',   --  77 instances
    'wikiquote',   --  72 instances
    'wikisource',  --  72 instances
    'wikinews',    --  30 instances
    'wikivoyage',  --  25 instances
    'wikiversity', --  17 instances
    'commons',     --   1 instance
    'wikidata',    --   1 instance
    'meta',        --   1 instance
    'mediawiki'    --   1 instance
    -- total          783 instances (as of June 2023)
  )
"""

#------------ CLASSES ------------#
# arguments needed for a single run of this job
class GeoEditorsArgs:
    def __init__(self, time_start, output_table):
        self.time_start = time_start
        self.output_table = output_table

#------------ HELPER FUNCTIONS ------------#
# Assigns buckets to raw edit counts for apples-to-apples error calculation 
def bucketize(df, input_col, bin_col):
    # Create a bucketizer instance
    bucketizer = Bucketizer(splits=splits, inputCol=input_col, outputCol=bin_col)

    # Apply bucketizer to the DataFrame
    df = bucketizer.transform(df)

    # Create an edit_range int col based on bucketizer
    df = df.withColumn(
        bin_col,
        sf.when(sf.isnan(bin_col),
        len(labels)-1).otherwise(sf.col(bin_col))
    )

    # Define the when conditions and corresponding labels
    when_conditions = [sf.col(bin_col) == i for i in range(len(labels))]
    when_labels = [sf.lit(labels[i]) for i in range(len(labels))]
    # Replace int col with correponding labels
    for i in range(len(when_conditions)):
        df = df.withColumn(
            bin_col,
            sf.when(
                when_conditions[i],
                when_labels[i]
            ).otherwise(sf.col(bin_col))
        )
        
    return df

def run_dp_count(
    df,
    keyset_df,
    epsilon,
    input_col,
    bin_col,
    private_output_col,
    nonprivate_output_col
):
    # Create keyset
    ks = KeySet.from_dataframe(keyset_df)
    
    # cache df for performance
    df.cache()
    df.take(1)
    
    # Create session
    session = Session.from_dataframe(
        privacy_budget=PureDPBudget(epsilon),
        source_id="geoeditors_count",
        dataframe=df,
        protected_change=AddOneRow()
    )
    
    # Define query
    query = (
        QueryBuilder("geoeditors_count")
        .bin_column(input_col, edit_binspec, name=bin_col)
        .groupby(ks)
        .count()
    )

    # Run the query
    private = session.evaluate(query, PureDPBudget(epsilon=epsilon))
    
    # Clean-up outputs
    private = private.withColumnRenamed(f'count', private_output_col)
    private = private.withColumn(private_output_col, sf.round(sf.col(private_output_col)))
    
    # Bucketize the dataframe
    nonprivate_df = bucketize(df, input_col=input_col, bin_col=bin_col)
    nonprivate_df.cache()
    nonprivate_df.take(1)
    
    # Nonprivate aggregation
    nonprivate = nonprivate_df.groupby(['country_code', 'wiki_db', bin_col]).count()
    nonprivate = nonprivate.withColumnRenamed(f'count', nonprivate_output_col)

    return private, nonprivate

# strategy:
# - add bin column
# - split by bins into separate dfs
# - for each df:
#  - group by country-project and sum, with bin min as lower bound and bin max as upper bound
# - concatenate dfs back together
def run_dp_sum(
    df,
    keyset_df,
    epsilon,
    input_col,
    bin_col,
    private_output_col,
    nonprivate_output_col
):
    # Bucketize the dataframe
    df = bucketize(df, input_col=input_col, bin_col=bin_col)
    df.cache()
    df.take(1)
    
    # calculate nonprivate agg
    nonprivate = df.groupby(['country_code', 'wiki_db', bin_col]).sum(input_col)
    nonprivate = nonprivate.withColumnRenamed(f'sum({input_col})', nonprivate_output_col)

    # Create meta session
    session = Session.from_dataframe(
        privacy_budget=PureDPBudget(float('inf')),
        source_id="geoeditors_sum",
        dataframe=df,
        protected_change=AddOneRow()
    )

    # For each df, define lo, hi, contrib_min, and contrib_max
    s = {}
    contrib_min_max = {}
    for i, l in enumerate(labels):
        lo = contrib_min = int(splits[i])
        if splits[i+1] == float('inf'):
            hi = str(splits[i+1])
            contrib_max = splits[i] + 1
        else:
            hi = contrib_max = int(splits[i+1])
            
        s[f'geoeditors_{lo}_{hi}'] = l
        contrib_min_max[f'geoeditors_{lo}_{hi}'] = [contrib_min, contrib_max]

    sessions = session.partition_and_create(
        "geoeditors_sum",
        privacy_budget=PureDPBudget(epsilon),
        column=bin_col,
        splits=s
    )
    
    private_dfs = []
    for i, (name, sess) in enumerate(sessions.items()):
        # create keyset
        filter_keyset_df = keyset_df.filter(keyset_df.edit_range == labels[i])
        ks = KeySet.from_dataframe(filter_keyset_df)
        # Define query as a groupby-sum
        query = (
            QueryBuilder(name)
            .groupby(ks)
            .sum(column=input_col, low=contrib_min_max[name][0], high=contrib_min_max[name][1])
        )
        
        # Run the query
        private = sess.evaluate(query, PureDPBudget(epsilon=epsilon))
        
        # Add the bucket back to the private output and append to output list
        private = private.withColumn(bin_col, sf.lit(labels[i]))
        private_dfs.append(private)
        sess.stop()

    # Union all private dfs together
    private = private_dfs[0]
    for df in private_dfs[1:]:
        private = private.union(df)
        
    private = private.withColumnRenamed(f'{input_col}_sum', private_output_col)
    private = private.withColumn(private_output_col, sf.round(sf.col(private_output_col)))

    return private, nonprivate

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Specify context for differential privacy aggregation of editors_daily'
    )
    parser.add_argument('time_start', help='month/week start date (in YYYY-MM-DD form)')
    parser.add_argument('output_table', help='table to write output to')
    return parser.parse_args()

#------------ MAIN FUNCTION ------------#
def do_dp_aggregation(spark, df, keyset_df, data_risk_classification, time_frame, time_start, log):
    release_thresh = param_dict[data_risk_classification]['release_thresh']

    # run initial count
    log.info('running private count aggregation...')
    private_count, nonprivate_count = run_dp_count(
        df,
        keyset_df=keyset_df,
        epsilon=param_dict[data_risk_classification]['count_eps'],
        input_col=input_cols[0],
        bin_col=bin_col,
        private_output_col=private_output_cols[0],
        nonprivate_output_col=nonprivate_output_cols[0]
    )
    
    log.info(f'filtering initial private count aggregation to just ≥ {release_thresh}...')
    private_count = private_count.filter(private_count.private_count >= release_thresh)

     # calculate errors
    log.info('calculating count errors...')
    errs = [
        calc_error_geoeditors(
            private_count,
            nonprivate_count,
            noisy_col=private_output_cols[0],
            exact_col=nonprivate_output_cols[0],
            release_thresh=release_thresh
        )
    ]
    
    if data_risk_classification == "Lower risk":
        # get keyset for subsequent aggregations
        log.info('getting keyset for sum...')
        sum_keyset_df = (
            private_count.filter(private_count.private_count >= release_thresh)
            .select(join_keys)
        )
        
        # run private sum
        log.info('running sum...')
        private_sum, nonprivate_sum = run_dp_sum(
            df,
            keyset_df=sum_keyset_df,
            epsilon=param_dict[data_risk_classification]['sum_eps'],
            input_col=input_cols[0],
            bin_col=bin_col,
            private_output_col=private_output_cols[1],
            nonprivate_output_col=nonprivate_output_cols[1]
        )

        # calc sum error and append to err list
        log.info('calculating sum error...')
        errs.append(
            calc_error_geoeditors(
                private_sum,
                nonprivate_sum,
                noisy_col=private_output_cols[1],
                exact_col=nonprivate_output_cols[1],
            )
        )
    
        # join all count and sum dfs together and fill negatives/nas with 0
        log.info('joining count and sum dfs together...')
        private = (
            private_count.join(private_sum, on=join_keys, how='outer')
            .withColumn(
                'private_sum',
                sf.when(
                    sf.col('private_sum') < 0,
                    0
                ).otherwise(sf.col('private_sum').cast('int'))
            )
        )

        # calculate total and pct inconsistent
        log.info('calculating inconsistencies...')
        total_inconsistent = (
            private.filter(
                ((private.private_count > 0) & (private.private_sum == 0))
            ).count()
        )
        pct_inconsistent = total_inconsistent / private.count()
        
        log.info('adding inconsistencies to error log...')
        for err in errs:
            err[time_frame] = time_start
            err['total_inconsistent'] = total_inconsistent
            err['pct_inconsistent'] = pct_inconsistent
    
    # for medium and higher risk countries, no sums, so fill that column in with None
    else:
        private = private_count.withColumn('private_sum', sf.lit(None))

        for err in errs:
            err[time_frame] = time_start
            err['total_inconsistent'] = None
            err['pct_inconsistent'] = None

    # add metadata to private table
    private = (
        private.withColumn(time_frame, sf.lit(time_start))
        .withColumn('count_eps', sf.lit(param_dict[data_risk_classification]['count_eps']))
        .withColumn('sum_eps', sf.lit(param_dict[data_risk_classification]['sum_eps']))
        .withColumn('count_release_thresh', sf.lit(release_thresh))
    )
    private = (
        private.withColumn('edit_range', 
            sf.when(private.edit_range == '[0.00, 4.00]', '1 to 4')
            .when(private.edit_range == '(4.00, 99.00]', '5 to 99')
            .when(private.edit_range == '(99.00, inf]', '100 or more')
        )
        .select(
            ['country_code', 'wiki_db', 'edit_range',
             'count_eps', 'sum_eps', 'count_release_thresh',
             'private_count', 'private_sum', time_frame]
        )
    )
        
    log.info('converting errors to spark df...')
    errs = spark.createDataFrame(
        [(
            data_risk_classification,
            err['name'],
            err['released'],
            err['med_rel_err'],
            err['dropped'],
            err['spurious'],
            err['rel_err_fraction_0_5'],
            err['rel_err_fraction_0_9'],
            err['total_inconsistent'],
            err['pct_inconsistent'],
            err[time_frame]
        ) for err in errs],
        schema=error_schema
        )
    
    log.info('done')
    return private, errs
